import 'package:flutter/material.dart';

class FirstRoute extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('First Route'),
      ),
      body: MyStatefulWidget(),
    );
  }
}

class _MyStatefulWidgetState extends State<MyStatefulWidget> {
  final _formKey = GlobalKey<FormState>();
  String name = '';
  int age;

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          TextFormField(
              decoration: const InputDecoration(
              icon: Icon(Icons.person),
              hintText: 'Who are u?',
              labelText: 'Name *',
            ),
            validator: (value) {
              if (value.isEmpty) {
                return 'Please enter some text';
              }
              print(value);
              setState(() {
                name = value;
              });
              this.name = value;
              return null;
            },
          ),
            TextFormField(
              decoration: const InputDecoration(
              icon: Icon(Icons.confirmation_number),
              hintText: 'How old are you?',
              labelText: 'Age *',
            ),
            validator: (value) {
              if (value.isEmpty) {
                return 'Please enter some text';
              }
              print(value);
              setState(() {
                age =  int.parse(value);
              });

              this.age = age;
              return null;
            },
            onChanged: (value) {
            },
          ),
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 16.0),
            child: RaisedButton(
              onPressed: () {
                // Validate will return true if the form is valid, or false if
                // the form is invalid.
                if (_formKey.currentState.validate()) {
                  // Process data.
                }
              },
              child: Text('Submit'),
            ),
          ),
          Center(
            child: Column(children: <Widget>[
              Text('Name: ' + this.name),
              (this.age != null) ? Text('Age: ' + this.age.toString()): Text(''),
            ],),
            // child: Text('Name: ' + this.name),
          ),
        ],
      ),
    );
  }
}

class MyStatefulWidget extends StatefulWidget {
  MyStatefulWidget({Key key}) : super(key: key);

  @override
  _MyStatefulWidgetState createState() => _MyStatefulWidgetState();
}